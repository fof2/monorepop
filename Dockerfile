FROM node:16
ARG SERVICE
ENV SERVICE ${SERVICE}
WORKDIR /home/usr/app
COPY . .
RUN npm ci
CMD npx nx serve ${SERVICE}
