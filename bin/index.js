const { writeFileSync } = require('fs');

const createBaseFile = () => `
stages:
  - publish
`;

const createBaseFileWithDeployment = () => `
image: ubuntu:18.04
stages:
  - publish
  - deploy
`;

const createEmptyJob = () => `
publish:empty:
  stage: publish
  script:
    - echo 'no apps affected!'
`;

const createJob = (service) => `
publish:${service}:
  stage: publish
  image: docker
  variables:
    SERVICE: ${service}
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build --pull --cache-from $CI_REGISTRY_IMAGE --build-arg SERVICE=${service} --tag $CI_REGISTRY_IMAGE/${service}:$CI_COMMIT_REF_SLUG .
    - docker push $CI_REGISTRY_IMAGE/${service}:$CI_COMMIT_REF_SLUG
`;

const createDeploy = (service) => `
deploy:${service}:
  stage: deploy
  image: alpine   
  needs:
    - publish:${service}
  variables:
    SERVICE: ${service}
    TOKEN_ACCESS: $TOKEN_ACCESS
  services:
    - name: docker:dind
      alias: docker
  before_script:
  - apk update
  - apk add git --no-cache
  - apk add yq --no-cache
  - git config --global user.email "alejandrorodriguezpena@gmail.com"
  - git config --global user.name "Alejandro-bot"
  script:
    - mkdir pipe && cd pipe
    - git init && git remote add jorigin https://oauth2:$TOKEN_ACCESS@gitlab.com/fof2/infra.git
    - git pull jorigin master
    - export image="registry.gitlab.com/fof2/monorepop/${service}:$CI_COMMIT_REF_SLUG"
    - yq -i '.spec.template.spec.containers[0].image= strenv(image)' dev/${service}.deployment.yaml
    - git add . && git commit -m "push back from pipeline in ${service}:$CI_COMMIT_REF_SLUG"
    - git push jorigin HEAD:master -o ci.skip 
`;

const createCIFile = (projects) => {
  if (!projects.length) {
    return createBaseFile().concat(createEmptyJob());
  }

  const publish = createBaseFileWithDeployment().concat(
    projects.map(createJob).join('\n')
  );

  const deploy = projects.map(createDeploy).join('\n');

  return publish.concat(deploy);
};

const createDynamicGitLabFile = () => {
  const [stringifiedAffected] = process.argv.slice(2);

  const { projects } = JSON.parse(stringifiedAffected);
  /*
  {
    projects: ['app1', 'app2'],
  };

  */
  const content = createCIFile(projects);

  writeFileSync('dynamic-gitlab-ci.yml', content);
};

createDynamicGitLabFile();
